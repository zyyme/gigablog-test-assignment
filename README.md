# Simple Blog MVP API

## Установка

### Docker

```shell
git clone https://gitlab.com/zyyme/gigablog-test-assignment.git

docker-compose build

docker-compose up -d
```


### Заполнение базы данных

Для заполнения базы данных можно воспользоваться набором данных из `populate_database.sql`:

```sql
CREATE DATABASE gigachat;

\c gigachat

-- populate_database.sql code
```

## Описание API 

### Articles

**POST /articles** - создание статьи, доступно только авторизованным пользователям, тело запроса (json):

```json
{
  "title": "string",
  "text": "string",
  "is_enabled": true
}
```

**GET /articles** - получение всех статей с пагинацией, принимает page и limit в query запроса для пагинации
 
**GET /articles/<article_id>** - получение статьи с комментариями по id

**PUT /articles/<article_id>** - изменение статьи по id, доступно только автору статьи. 

**DELETE /articles/<article_id>** - удаление статьи по id

### Comments

**POST /comments** - создание комментария, доступно только авторизованным пользователям, тело запроса (json):

```json
{
  "article": "article_id",
  "text": "string"
}
```

**GET /comments/<comment_id>** - получение комментария по id.

**PUT /comments/<comment_id>** - изменение комментария по id.

**DELETE /comments/<comment_id>** - удаление комментария по id.

### Tokens

**POST /tokens/** - создание токена для пользователя, тело запроса (json):

```json
{
  "username": "string",
  "token": "string"
}
```

**POST /tokens/auth/** - аутентификация пользователя, тело запроса (json):

```json
{
  "username": "string",
  "token": "string"
}
```
