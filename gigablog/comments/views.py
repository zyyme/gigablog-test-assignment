import json

from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.http import Http404, HttpResponseForbidden, JsonResponse
from django.views import View

from .models import Comment


class GetAllOrCreateCommentView(View):
    def get(self, request):
        page = int(request.GET.get("page", 1))
        limit = int(request.GET.get("limit", 10))

        comments = Comment.objects.all()
        paginator = Paginator(comments, limit)
        comments_page = paginator.get_page(page)

        return JsonResponse(
            {
                "data": list(comments_page.object_list.values()),
                "page": page,
                "limit": limit,
                "pages": paginator.num_pages,
                "total": len(comments),
            }
        )

    def post(self, request):
        if (
            request.user.is_authenticated
        ):  # todo: custom decorator for this logic to return proper 403
            data = json.loads(request.body)
            data['author'] = User.objects.get(username=request.user)
            article = Comment(**data)
            article.save()
            return JsonResponse({"post": article})

        return HttpResponseForbidden(content="Not authenticated")


class SingleCommentView(View):
    def get(self, request, comment_id):
        article = Comment.objects.filter(
            pk=comment_id
        ).values()  # todo: probably there is less hacky way to do it
        if len(article) == 0:
            raise Http404()
        article = article[0]
        article["comments"] = list(
            Comment.objects.filter(article_id=comment_id).values()
        )

        return JsonResponse({"data": article})

    def put(self, request, comment_id: int):
        if request.user.is_authenticated:
            user = User.objects.get(username=request.user)
            comment = Comment.objects.get(pk=comment_id)

            if comment.author == user or user.is_superuser:
                changes = json.loads(request.body)
                Comment.objects.filter(id=comment_id).update(**changes)
                return JsonResponse({"success": True})
        return HttpResponseForbidden(content="Not authenticated")

    def delete(self, request, comment_id: int):
        if request.user.is_authenticated:
            user = User.objects.get(username=request.user)
            comment = Comment.objects.get(pk=comment_id)

            if comment.author == user or user.is_superuser:
                Comment.objects.filter(id=comment_id).delete()
                return JsonResponse({"success": True})
        return HttpResponseForbidden(content="Not authenticated")
