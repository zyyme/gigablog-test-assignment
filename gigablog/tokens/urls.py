from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path("", csrf_exempt(views.CreateToken.as_view()), name="create_token"),
    path("auth/", csrf_exempt(views.AuthWithToken.as_view()), name="auth_user"),
]
