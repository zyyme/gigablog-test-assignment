import json

from django.contrib.auth import login
from django.contrib.auth.models import User
from django.http import Http404, HttpResponseForbidden, JsonResponse
from django.views import View

from .models import Token


class CreateToken(View):
    def post(self, request):
        data = json.loads(request.body)
        user = User.objects.get(username=data["username"])
        if user:
            data["user"] = user
            token = Token(**data)
            token.save()

            return JsonResponse({"success": True})

        raise Http404()


class AuthWithToken(View):
    def post(self, request):
        data = json.loads(request.body)

        user = User.objects.get(username=data["username"])

        if not user:
            raise Http404()

        tokens = Token.objects.filter(user=user)
        for token in tokens:
            if token.token == data["token"]:
                login(request, user)
                return JsonResponse({"success": True})

        return HttpResponseForbidden(content="Invalid data")
