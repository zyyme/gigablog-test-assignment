from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path(
        "",
        csrf_exempt(views.GetAllOrCreateArticleView.as_view()),
        name="get_post_articles",
    ),
    path(
        "<int:article_id>/",
        csrf_exempt(views.SingleArticleView.as_view()),
        name="get_one_update_delete_articles",
    ),
]
