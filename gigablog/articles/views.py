import json

from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.http import Http404, HttpResponseForbidden, JsonResponse
from django.views.generic import View

from comments.models import Comment

from .models import Article


class GetAllOrCreateArticleView(View):
    def get(self, request):
        page = int(request.GET.get("page", 1))
        limit = int(request.GET.get("limit", 10))

        articles = Article.objects.all()
        paginator = Paginator(articles, limit)
        articles_page = paginator.get_page(page)

        return JsonResponse(
            {
                "data": list(articles_page.object_list.values()),
                "page": page,
                "limit": limit,
                "pages": paginator.num_pages,
                "total": len(articles),
            }
        )

    def post(self, request):
        if (
            request.user.is_authenticated
        ):  # todo: custom decorator for this logic to return proper 403
            data = json.loads(request.body)
            data['author'] = User.objects.get(username=request.user)
            article = Article(**data)
            article.save()
            return JsonResponse({"post": article})

        return HttpResponseForbidden(content="Not authenticated")


class SingleArticleView(View):
    def get(self, request, article_id: int):
        article = Article.objects.filter(
            pk=article_id
        ).values()  # todo: probably there is less hacky way to do it
        if len(article) == 0:
            raise Http404()
        article = article[0]
        article["comments"] = list(
            Comment.objects.filter(article_id=article_id).values()
        )

        return JsonResponse({"data": article})

    def put(self, request, article_id: int):
        if request.user.is_authenticated:
            user = User.objects.get(username=request.user)
            article = Article.objects.get(pk=article_id)

            if article.author == user or user.is_superuser:
                changes = json.loads(request.body)
                Article.objects.filter(id=article_id).update(**changes)
                return JsonResponse({"success": True})
        return HttpResponseForbidden(content="Not authenticated")

    def delete(self, request, article_id: int):
        if request.user.is_authenticated:
            user = User.objects.get(username=request.user)
            article = Article.objects.get(pk=article_id)

            if article.author == user or user.is_superuser:
                Article.objects.filter(id=article_id).delete()
                return JsonResponse({"success": True})
        return HttpResponseForbidden(content="Not authenticated")
