INSERT INTO auth_user ( -- username: not_admin, password: admin
                       username,
                       password,
                       is_superuser,
                       first_name,
                       last_name,
                       email,
                       is_staff,
                       is_active,
                       date_joined
                       )
VALUES (
        'not_admin',
        'pbkdf2_sha256$600000$CAQESQrgdzIxAGwN8y7nvl$W3eKq2mZsBgeOmXlzyxOd6n5JS6cJCldnrMiHALUG58=',
        false,
        '',
        '',
        '',
        false,
        true,
        current_timestamp
        );

INSERT INTO articles_article (title, text, author_id, created_at, updated_at, is_published)
VALUES ('first article', 'lorem ipsom dolor sit amet', 1, current_timestamp, current_timestamp, true);

INSERT INTO comments_comment (text, author_id, article_id, created_at, updated_at)
VALUES ('first comment', 1, 1, current_timestamp, current_timestamp);

INSERT INTO tokens_token (token, user_id, is_active)
VALUES ('test_token', 1, true);